package model;

import java.util.Date;

/**
 * Created by GligaBogdan on 12-Mar-16.
 */
public class Student {

    private static int counter;

    private int id;
    private String name;
    private String address;
    private long birthDay;


    public Student(String name, String address, long birthDay) {
        this(counter,name,address,birthDay);
        counter++;
    }

    public Student(int id,String name, String address, long birthDay) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthDay = birthDay;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public long getBirthDay() {
        return birthDay;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", birthDay=" + birthDay +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (id != student.id) return false;
        if (birthDay != student.birthDay) return false;
        if (name != null ? !name.equals(student.name) : student.name != null) return false;
        if (address != null ? !address.equals(student.address) : student.address != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (int) (birthDay ^ (birthDay >>> 32));
        return result;
    }
}
