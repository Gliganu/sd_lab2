package dao;

import conf.ConnectionConfiguration;
import model.Course;
import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GligaBogdan on 15-Mar-16.
 */
public class CourseDaoImpl implements CourseDao {

    private StudentDao studentDao;

    public CourseDaoImpl() {
        studentDao = new StudentDaoImpl();
    }

    public void createCourseTable() {

        Connection connection = ConnectionConfiguration.getConnection();

        try {
            Statement courseStatement = connection.createStatement();
            courseStatement.execute("CREATE TABLE IF NOT EXISTS course (id INT PRIMARY KEY UNIQUE, " +
                    "name VARCHAR(55), teacher VARCHAR(55), study_year INTEGER)");
            courseStatement.close();

            Statement courseStudentStatement = connection.createStatement();
            courseStudentStatement.execute("CREATE TABLE IF NOT EXISTS course_student (course_id INTEGER," +
                    "student_id INTEGER)");
            courseStudentStatement.close();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteCourseTable() {


        Connection connection = ConnectionConfiguration.getConnection();

        try {
            Statement courseStatement = connection.createStatement();
            courseStatement.execute("DROP TABLE course");
            courseStatement.close();

            Statement courseStudentStatement = connection.createStatement();
            courseStudentStatement.execute("DROP TABLE course_student");
            courseStudentStatement.close();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int insert(Course course) {

        int returnStatus = -1;
        try {

            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO course (id, name, teacher, study_year)" +
                    "VALUES (?,?,?,?)");

            preparedStatement.setInt(1, course.getId());
            preparedStatement.setString(2, course.getName());
            preparedStatement.setString(3, course.getTeacher());
            preparedStatement.setInt(4, course.getStudyYear());

            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;
    }

    public int delete(Course course) {
        int returnStatus = -1;

        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM course WHERE id = ?");

            preparedStatement.setInt(1, course.getId());
            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;
    }

    public int update(Course course, int id) {

        int returnStatus = -1;

        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE course SET " +
                    "name = ?, teacher = ?, study_year = ? WHERE id = ?");

            preparedStatement.setString(1, course.getName());
            preparedStatement.setString(2, course.getTeacher());
            preparedStatement.setInt(3, course.getStudyYear());
            preparedStatement.setInt(4, id);

            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;
    }

    public int enrollStudent(Student student, Course course) {

        int returnStatus = -1;
        try {
            Connection connection = ConnectionConfiguration.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO course_student (course_id, student_id)" +
                    "VALUES (?,?)");

            preparedStatement.setInt(1, course.getId());
            preparedStatement.setInt(2, student.getId());

            returnStatus = preparedStatement.executeUpdate();

            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnStatus;

    }

    public List<Student> getAllEnrolledStudents(Course course) {

        List<Student> students = new ArrayList<Student>();

        Connection connection = ConnectionConfiguration.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT  * FROM course_student WHERE course_id = ?");
            preparedStatement.setInt(1, course.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int studentId = resultSet.getInt("student_id");
                Student student = studentDao.selectById(studentId);
                students.add(student);
            }

            resultSet.close();
            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return students;
    }

    public Course selectById(int id) {

        Course course = null;
        Connection connection = ConnectionConfiguration.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM course WHERE id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();

            int receivedId = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String teacher = resultSet.getString("teacher");
            int studyYear = resultSet.getInt("study_year");

            course = new Course(receivedId, name, teacher, studyYear);

            resultSet.close();
            preparedStatement.close();
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return course;
    }

}
