package conf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by GligaBogdan on 12-Mar-16.
 */
public class ConnectionConfiguration {


    public static Connection getConnection(){

        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sd_schema","root","root");

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return connection;

    }
}
