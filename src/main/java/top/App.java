package top;

import dao.CourseDao;
import dao.CourseDaoImpl;
import dao.StudentDao;
import dao.StudentDaoImpl;
import model.Course;
import model.Student;

import java.util.Date;
import java.util.List;

/**
 * Created by GligaBogdan on 12-Mar-16.
 */
public class App {


    public static void main(String[] args) {


        StudentDao studentDao = new StudentDaoImpl();
        CourseDao courseDao = new CourseDaoImpl();

        studentDao.deleteStudentTable();
        courseDao.deleteCourseTable();

        studentDao.createStudentTable();
        courseDao.createCourseTable();

        Student s1 = new Student("A", "T", System.currentTimeMillis());
        Student s2 = new Student("B", "T", System.currentTimeMillis());
        Student s3 = new Student("C", "T", System.currentTimeMillis());
        Student s4 = new Student("D", "T", System.currentTimeMillis());

        studentDao.insert(s1);
        studentDao.insert(s2);
        studentDao.insert(s3);
        studentDao.insert(s4);

        Course c1 = new Course("C1","T",1);
        Course c2 = new Course("C2","T",1);

        courseDao.insert(c1);
        courseDao.insert(c2);

        courseDao.enrollStudent(s1,c1);
        courseDao.enrollStudent(s2,c1);
        courseDao.enrollStudent(s3,c2);
        courseDao.enrollStudent(s4,c2);


        System.out.println(courseDao.getAllEnrolledStudents(c1));

    }
}
