package dao;

import conf.ConnectionConfiguration;
import model.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by GligaBogdan on 15-Mar-16.
 */
public class StudentDaoImplTest {

    private StudentDao studentDao;

    private Student s1;


    @Before
    public void setUp() throws Exception {

        Connection connection = ConnectionConfiguration.getConnection();
        studentDao = new StudentDaoImpl();

        studentDao.createStudentTable();

        s1 = new Student("A", "A1", System.currentTimeMillis());
        Student s2 = new Student("B", "A2", System.currentTimeMillis());
        Student s3 = new Student("C", "A3", System.currentTimeMillis());
        Student s4 = new Student("D", "A4", System.currentTimeMillis());

        studentDao.insert(s1);
        studentDao.insert(s2);
        studentDao.insert(s3);
        studentDao.insert(s4);

    }

    @After
    public void tearDown() throws Exception {

        studentDao.deleteStudentTable();
    }


    @Test
    public void testInsert() throws Exception {

        Student insertStudent = new Student("Test", "Test", System.currentTimeMillis());

        studentDao.insert(insertStudent);

        Student retrievedStudent = studentDao.selectById(insertStudent.getId());

        assertEquals(retrievedStudent,insertStudent);


    }

    @Test
    public void testSelectById() throws Exception {

        Student retrievedStudent = studentDao.selectById(s1.getId());

        assertEquals(retrievedStudent,s1);

    }

    @Test
    public void testDelete() throws Exception {

        int deleteStatus = studentDao.delete(s1);

        assertEquals(deleteStatus,1);
    }

    @Test
    public void testUpdate() throws Exception {

        s1.setAddress("TestAddress");
        int updateStatus = studentDao.update(s1, s1.getId());

        assertEquals(updateStatus,1);

        Student retrievedStudent = studentDao.selectById(s1.getId());

        assertEquals(retrievedStudent,s1);
    }
}