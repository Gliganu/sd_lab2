package dao;

import conf.ConnectionConfiguration;
import model.Course;
import model.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by GligaBogdan on 15-Mar-16.
 */
public class CourseDaoImplTest {

    private CourseDao courseDao;
    private StudentDao studentDao;
    private Connection connection;
    private Course c1;
    private Course c2;

    @Before
    public void setUp() throws Exception {
        connection = ConnectionConfiguration.getConnection();

        courseDao = new CourseDaoImpl();
        studentDao = new StudentDaoImpl();

        courseDao.createCourseTable();
        studentDao.createStudentTable();

        Student s1 = new Student("A", "A1", System.currentTimeMillis());
        Student s2 = new Student("B", "A2", System.currentTimeMillis());
        Student s3 = new Student("C", "A3", System.currentTimeMillis());
        Student s4 = new Student("D", "A4", System.currentTimeMillis());


        studentDao.insert(s1);
        studentDao.insert(s2);
        studentDao.insert(s3);
        studentDao.insert(s4);

        c1 = new Course("C1","T",1);
        c2 = new Course("C2","T",1);

        courseDao.enrollStudent(s1, c1);
        courseDao.enrollStudent(s2, c1);
        courseDao.enrollStudent(s3, c2);
        courseDao.enrollStudent(s4, c2);


    }

    @After
    public void tearDown() throws Exception {

        studentDao.deleteStudentTable();
        courseDao.deleteCourseTable();
    }



    @Test
    public void testInsert() throws Exception {

        Course course = new Course("Test","TestTeacher",1);

        int insertStatus = courseDao.insert(course);

        Course receivedCourse = courseDao.selectById(course.getId());

        assertEquals(course,receivedCourse);
        assertEquals(insertStatus,1);

    }

    @Test
    public void testDelete() throws Exception {

        Course course = new Course("Test","TestTeacher",1);
        courseDao.insert(course);

        int deleteStatus = courseDao.delete(course);

        assertEquals(deleteStatus,1);
    }

    @Test
    public void testUpdate() throws Exception {
        Course course = new Course("Test","TestTeacher",1);
        courseDao.insert(course);

        course.setName("Test_2");

        int updateStatus = courseDao.update(course, course.getId());

        assertEquals(updateStatus,1);

        Course receivedCourse = courseDao.selectById(course.getId());

        assertEquals(course,receivedCourse);

    }

    @Test
    public void testEnrollStudent() throws Exception {

        Student s1 = new Student("EnrolS", "EnrolA", System.currentTimeMillis());
        studentDao.insert(s1);

        int enrollStatus = courseDao.enrollStudent(s1, c1);

        assertEquals(enrollStatus,1);

    }

    @Test
    public void testGetAllEnrolledStudents() throws Exception {

        List<Student> c1s = courseDao.getAllEnrolledStudents(c1);
        List<Student> c2s = courseDao.getAllEnrolledStudents(c2);

        assertEquals(c1s.size(),2);
        assertEquals(c2s.size(),2);
    }
}